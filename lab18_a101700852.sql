SET DATEFORMAT dmy
--a)
SELECT sum(cantidad) As 'cantidad1997', sum((cantidad * ((costo * PorcentajeImpuesto/100)+costo))) as 'importeTotal'
FROM Entregan e, Materiales m
WHERE e.clave = m.clave AND e.fecha between '01/01/1997' AND '31/12/1997'

SELECT RazonSocial, count(e.RFC) as 'numeroEntregas', sum((cantidad * ((costo * PorcentajeImpuesto/100)+costo))) as 'importeTotal'
FROM Proveedores pr, Materiales m, Entregan e
WHERE e.clave = m.clave and e.RFC = pr.RFC
GROUP BY RazonSocial

SELECT m.clave, m.descripcion, sum(cantidad) as 'cantidadTotal', min(cantidad) as 'cantidadMinima', max(cantidad) as 'cantidadMaxima', sum((cantidad * (costo + (costo * PorcentajeImpuesto/100)))) as 'importeTotal'
FROM Materiales m, Entregan e
WHERE e.clave = m.clave
GROUP BY m.clave, descripcion
HAVING avg(e.cantidad) > 400

SELECT pr.RazonSocial, m.clave, m.Descripcion, avg(cantidad) as 'cantidadPromedio'
FROM Proveedores pr, Materiales m, Entregan e
WHERE e.clave = m.clave AND pr.RFC = e.RFC
GROUP BY RazonSocial, m.clave, Descripcion
HAVING avg(cantidad) <= 499

SELECT pr.RazonSocial, m.clave, m.Descripcion, avg(cantidad) as 'cantidadPromedio'
FROM Proveedores pr, Materiales m, Entregan e
WHERE e.clave = m.clave AND pr.RFC = e.RFC
GROUP BY RazonSocial, m.clave, m.descripcion
HAVING  avg(cantidad) < 370 OR avg(cantidad) > 450



--b)
INSERT INTO Materiales VALUES (1234, 'cemento azul', 100.00, 2.5);
INSERT INTO Materiales VALUES (1235, 'cemento blanco', 117.00, 2.6);
INSERT INTO Materiales VALUES (1236, 'cemento gris', 89.00, 2.7);
INSERT INTO Materiales VALUES (1237, 'cemento perdura', 131.00, 2.8);
INSERT INTO Materiales VALUES (1238, 'cemento cemex', 130.00, 2.9);

SELECT clave, descripcion
FROM Materiales
WHERE clave NOT IN
        (
        SELECT m.clave
        FROM Materiales m, Entregan e
        WHERE m.clave = e.clave
         )

SELECT DISTINCT RazonSocial
FROM Proveedores pr, Proyectos p, Entregan e
WHERE p.numero = e.numero AND pr.RFC = e.RFC AND Denominacion = 'Vamos Mexico'
      AND RazonSocial IN
       (
        SELECT  RazonSocial
        FROM Proveedores pr, Proyectos p, Entregan e
        WHERE p.numero = e.numero AND pr.RFC = e.RFC AND Denominacion like 'Queretaro Limpio'
        )

SELECT descripcion
FROM Materiales m, Entregan e, Proyectos p
WHERE m.clave = e.clave AND p.numero = e.numero AND Descripcion NOT IN (
                            SELECT descripcion
                            FROM Materiales m, Entregan e, Proyectos p
                            WHERE e.clave = m.clave AND p.numero = e.numero AND p.denominacion like 'CIT Yucatan'
                          )

SELECT razonsocial, avg(cantidad) AS 'cantidadPromedio'
FROM  Proveedores pr, Entregan e
WHERE e.RFC = pr.RFC
GROUP BY RazonSocial
HAVING avg(cantidad) > (SELECT avg(cantidad)
	                          FROM Proveedores pr, Entregan e
	                          WHERE pr.RFC = e.RFC AND p.RFC like'%VAGO0780901%'
                        )

SELECT RazonSocial,e.RFC, sum(cantidad) as 'cantidadTotal'
FROM Entregan e, Proveedores pr, Proyectos p
WHERE e.RFC=pr.RFC and e.numero=p.numero and p.denominacion like '%Infonavit Durango%' AND fecha between '01/01/01' AND '31/12/01'
GROUP BY e.rfc, razonsocial
HAVING sum(cantidad) > (SELECT sum(cantidad) as 'cantidadTotal'
	FROM Entregan e, Proveedores pr, Proyectos p
	WHERE pr.RFC = e.RFC AND p.numero = e.numero AND p.denominacion like 'Infonavit Durango' AND fecha between '01/01/00' AND '31/12/00'
)
