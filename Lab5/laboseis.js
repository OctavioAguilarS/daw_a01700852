//Ejercicio Dos
var ejercicioDos = document.getElementById('cambioColor');

ejercicioDos.onmouseover = function () {
    ejercicioDos.style.color = "blueviolet";
}
ejercicioDos.onmouseout = function () {
    ejercicioDos.style.color = "white";
}

//Ejercicio tres

var ejercicioTres = document.getElementById('informacion');
ejercicioTres.onmouseover = function () {
    informacion.style.display = "block";
}
ejercicioTres.onclick = function () {
   informacion.style.display = "none";
}

//Ejercicio Cuatro
function load() {
  var elem = document.getElementById("carga");   
  var width = 0;
  var id = setInterval(frame, 5);
  function frame() {
    if (width == 100) { //Este es el porcentaje a llenar de la barra
      clearInterval(id);
    } else { 
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}

//Ejercicio Cinco
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}