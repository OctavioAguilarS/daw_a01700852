BULK INSERT a1700852.a1700852.[Proyectos]
   FROM 'e:\wwwroot\a1700852\proyectos.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )