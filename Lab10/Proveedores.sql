BULK INSERT a1700852.a1700852.[Proveedores]
   FROM 'e:\wwwroot\a1700852\proveedores.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )