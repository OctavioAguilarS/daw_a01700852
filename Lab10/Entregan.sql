BULK INSERT a1700852.a1700852.[Entregan]
   FROM 'e:\wwwroot\a1700852\entregan.csv'
   WITH
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )