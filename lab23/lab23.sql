CREATE table  ClienteBanco
(
	noCuenta varchar(5) primary key,
	nombre varchar(30),
	saldo numeric(10,2)
)

CREATE table TipoMovimiento
(
	claveM varchar(2) primary key,
	descripcion varchar(30)
)

CREATE table Realizan
(
	noCuenta varchar(5) FOREIGN KEY (noCuenta) REFERENCES ClienteBanco(noCuenta),
	claveM varchar(2) FOREIGN KEY (claveM) REFERENCES TipoMovimiento(claveM),
	fecha datetime,
	monto numeric(10,2)
)
/*
    Prueba 1
*/
BEGIN TRANSACTION PRUEBA1
INSERT INTO ClienteBanco VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO ClienteBanco VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO ClienteBanco VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1

SELECT * FROM ClienteBanco

  /*
    Prueba 2
*/

BEGIN TRANSACTION PRUEBA2
INSERT INTO ClienteBanco VALUES('004','Ricardo Rios Maldonado',19000;
INSERT INTO ClienteBanco VALUES('005','Pablo Ortiz Arana',15000);
INSERT INTO ClienteBanco VALUES('006','Luis Manuel Alvarado',18000);

SELECT * FROM ClienteBanco


ROLLBACK TRANSACTION PRUEBA2
SELECT * FROM ClienteBanco

/*
    Prueba 3
*/
BEGIN TRANSACTION PRUEBA3
INSERT INTO TipoMovimiento VALUES('A','Retiro Cajero Automatico');
INSERT INTO TipoMovimiento VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3
/*
    Prueba 4
*/

BEGIN TRANSACTION PRUEBA4
INSERT INTO Realizan VALUES('001','A',GETDATE(),500);
UPDATE ClienteBanco SET saldo = saldo - 500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4

SELECT * FROM ClienteBanco
SELECT * FROM TipoMovimiento
SELECT * FROM Realizan


/*
    Prueba 5
*/
BEGIN TRANSACTION PRUEBA5
INSERT INTO ClienteBanco VALUES('005','Rosa Ruiz Maldonado',9000
INSERT INTO ClienteBanco VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO ClienteBanco VALUES('001','Oscar Perez Alvarado',8000);
IF @@ERROR = 0
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END
SELECT * FROM ClienteBanco

/*
    retiro cajero
*/

CREATE PROCEDURE REGISTRAR_RETIRO_CAJERO
	@noCuenta  varchar(5),
	@monto     numeric(10,2)
AS
	BEGIN TRANSACTION RetiroCajero
INSERT INTO Realizan VALUES (@noCuenta,'A',getdate(),@monto);
UPDATE ClienteBanco SET saldo = saldo - @monto where noCuenta = @noCuenta
	IF @@ERROR = 0
COMMIT TRANSACTION RetiroCajero
  ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
    ROLLBACK TRANSACTION RetiroCajero
	END
GO

EXECUTE REGISTRAR_RETIRO_CAJERO '001', 1300.00

SELECT * FROM ClienteBanco
SELECT * FROM Realizan

/*
    deposito en ventanilla
*/
CREATE PROCEDURE REGISTRAR_DEPOSITO_VENTANILLA
	@noCuenta  varchar(5),
	@monto     numeric(10,2)
AS
	BEGIN TRANSACTION DepositoVentanilla
INSERT INTO Realizan VALUES (@noCuenta, 'B', getdate(), @monto);
	UPDATE ClienteBanco SET saldo = saldo + @monto where noCuenta = @noCuenta
	IF @@ERROR = 0
COMMIT TRANSACTION DepositoVentanilla
  ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION DepositoVentanilla
	END
GO

EXECUTE REGISTRAR_DEPOSITO_VENTANILLA '001',1300.00

SELECT * FROM ClienteBanco
SELECT * FROM Realizan