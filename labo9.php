<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Lab9_PHP</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
<h4>Octavio Aguilar Sanchez / A01700852 ISC</h4>
<H1>LABORATORIO 9: Utilizando PHP y XAMPP</H1>

<!--Comienzan: Problema 1-->


<h2>Promedio de: (10,11,12,13,14,15,16,17,18,19,20,21) :</h2>
<p>EJERCICIO 1: </p>
<?php 
function calcularPromedio($valores){

  $valorTotal = 0;
  for($i=0;$i<count($valores);$i++){
    $valorTotal += $valores[$i];
  }

  $valores = count($valores);
  $calcularPromedio = $valorTotal/$valores;
  return $calcularPromedio;
}
  echo ("El promedio es: ");
  echo calcularPromedio([10,11,12,13,14,15,16,17,18,19,20,21]);
  ?>


<!--Comienzan: Problema 2-->
<br><br>
<p>EJERCICIO 2: </p>

<?php
function calcularMediana($valores) {
    $count = count($valores);   
    $valorMedio = floor(($count-1)/2); 
    if($count % 2) { 
        $mediana = $valores[$valorMedio];
    } else { 
      $mayor = $valores[$valorMedio+1];
        $menor = $valores[$valorMedio];
        $mayor = $valores[$valorMedio+1];
        $mediana = (($mayor+$menor)/2);
    }
    return $mediana;
}
    echo ("La mediana es:");
  echo calcularMediana([10,11,12,13,14,15,16,17,18,19,20,21]);
    
  ?>

<br>
<!--Comienzan: Problema 3-->
<p>EJERCICIO 3: </p>
<br>

<?php 

function generaLista($valores){

              
              for($i = 0 ; $i < count($valores) ; $i++){

                echo $valores[$i] . "<br>" ;
              }
             
            }

   echo generaLista([10,11,12,13,14,15,16,17,18,19,20,21]);
    echo ("La mediana es:");
  echo calcularMediana([10,11,12,13,14,15,16,17,18,19,20,21]);
  echo ("...");
  echo ("El promedio es: ");
    echo calcularPromedio([10,11,12,13,14,15,16,17,18,19,20,21]);
  $valores = [10,11,12,13,14,15,16,17,18,19,20,21] ;
  
    rsort($valores) ;
    echo "<li> Valores ordenados de mayor a menor: ". implode("...", $valores) . "</li>";
    sort($valores) ;
    echo "<li>Valores ordenados de menor a mayor: " . implode("...", $valores) . "</li>";
      
 ?>

<!--Comienzan: Problema 4-->
<br>
<p>EJERCICIO 5: </p>
<!--Comienzan: Problema 5-->

<?php

  $min=1;
  $max=25;
  $dia = rand($min,$max);

  echo "EJERCICIO LIBRE:";

echo " El color de los ojos de tus hijos sera:";
if ($dia <= "5") {
    echo "CAFE";
} elseif ($dia >="5" && $dia <="10" ) {
    echo "VERDE";
} else if ($dia >= "11" && $dia <= "15" ){
    echo "AZUL";
} else if ($dia >= "16" && $dia <= "20" ){
    echo "GRIS";
} else if ($dia >= "21" && $dia <= "25" ){
    echo "MORADO!";
} 
?>







<!--Comienzan: PREGUNTAS y RESPUESTAS-->
  <p>
  <br><br>
  <h1>PREGUNTAS</h1>
  
  <ol>
 <li>¿Qué hace la función phpinfo()? Describe y discute 3 datos que llamen tu atención.<br>
“Muestra gran cantidad de información sobre el estado actual de PHP. Incluye información sobre las opciones de compilación y extensiones de PHP, versión de PHP, información del servidor y entorno (si se compiló como módulo), entorno PHP, versión del OS, rutas, valor de las opciones de configuración locales y generales, cabeceras HTTP y licencia de PHP. ” 1
<br>

Me llamo la atención: 

<br><br><ul>
  <li>INFO_LICENSE: este muestra la licencia de php utilizada. </li>
  <li>INFO_VARIABLES: este muestra las variables predefinidas de EGPCS. </li>
  <li>INFO_CREDITS: se muestran los créditos utilizados de php. </li>

</ul></li><br>

<li>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?
<br>
Acceder a la información que se maneja en la aplicación no es tan complicado, de manera que creo se tiene que aumentar la seguridad, por el momento no se cómo, pero espero que para el final del curso aprendamos una manera más segura de proteger el sitio en un ambiente de producción.
</li>
<br><br>
<li>¿Cómo es que, si el código está en un archivo con código HTML que se despliega del lado del cliente, se ejecuta del lado del servidor? Explica.
<br>
El código está en un archivo de código HTML, sin embargo, se realiza una solicitud al servidor para que este se encargue de interpretar el código y se devuelva esta información. Me parece una herramienta útil y bastante interesante. 
</li>
</ol>

 
<br>

<h5>1.  P. (n.d.). Phpinfo. Retrieved September 07, 2017, from http://php.net/manual/es/function.phpinfo.php<h5>
  </p>
  
  </body>
</html>
